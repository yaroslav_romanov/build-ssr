/* eslint-disable no-param-reassign */

import { createSlice, AnyAction } from '@reduxjs/toolkit';
import { AuthI } from 'typings/auth';
import initialState from './initialState';

const authReducer = createSlice({
  name: 'authReducer',
  initialState: initialState.auth,
  reducers: {
    authFetchRequesting: (state: AuthI) => {
      state.fetchingState = 'requesting';
    },
    authFetchSuccess: (state: AuthI, action: AnyAction) => {
      state.user = action.payload.user;
      state.fetchingState = 'success';
    },
    authFetchError: (state: AuthI, action: AnyAction) => {
      state.fetchingState = 'failed';
      state.error = action.payload.error;
    },
  },
});

const { actions, reducer } = authReducer;

export const {
  authFetchRequesting,
  authFetchSuccess,
  authFetchError,
} = actions;

export default reducer;
