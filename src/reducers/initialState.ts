const initialState = {
  auth: {
    fetchingState: 'none',
    creatingState: 'none',
    loadingState: 'none',
    error: null,
    user: {},
  },
};

export default initialState;
