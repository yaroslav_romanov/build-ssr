import {
  takeLatest, all, put, call,
} from 'redux-saga/effects';

import { authFetchRequesting, authFetchSuccess, authFetchError } from '@/reducers/auth';

const fetchAuthExternal = () => new Promise((resolve) => {
  setTimeout(() => resolve({
    user: {
      firstName: 'guest',
      lastName: 'guest',
    },
  }), 1000);
});

export function* fetchAuth() {
  try {
    const response = yield call(fetchAuthExternal);

    yield put({
      type: authFetchSuccess.type,
      payload: { user: response.user },
    });
  } catch (error) {
    yield put({
      type: authFetchError.type,
      payload: { error },
    });
  }
}

export default function* authSagas() {
  yield all([takeLatest(authFetchRequesting.type, fetchAuth)]);
}
