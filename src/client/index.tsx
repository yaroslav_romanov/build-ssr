import { loadableReady } from '@loadable/component';
import React from 'react';
import { hydrate } from 'react-dom';
import { HelmetProvider } from 'react-helmet-async';
import { Provider as StoreProvider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import { StoreI } from 'typings/store';

import App from '@/app';
import { getStore } from '@/store';
import rootSaga from '@/sagas';
import { ReactComponent as SpriteSvg } from '@/assets/img/sprite.svg';

// eslint-disable-next-line no-underscore-dangle
const store: StoreI = getStore(window.__PRELOADED_STATE__);

if (store.runSaga) {
  store.runSaga(rootSaga);
}

const render = () => {
  hydrate(
    <StoreProvider store={store}>
      <HelmetProvider>
        <BrowserRouter>
          <App />
          <SpriteSvg />
        </BrowserRouter>
      </HelmetProvider>
    </StoreProvider>,
    document.getElementById('root'),
  );
};

if (module.hot) {
  module.hot.accept('@/app', render);
}

loadableReady(() => render());
