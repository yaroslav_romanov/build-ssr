import { RouteProps } from 'react-router';
import { Saga } from 'redux-saga';

import {
  PageIndex, PageTest2, PageTest1, LayoutIndex,
} from '@/pages';

import { fetchAuth } from '@/sagas/auth';

export const routes: RouteType[] = [
  {
    path: '/',
    component: LayoutIndex,
    private: false,
    sagasToRun: [fetchAuth],
    routes: [
      {
        path: '/test2',
        exact: true,
        component: PageTest2,
      },
      {
        path: '/test1',
        exact: true,
        component: PageTest1,
      },
      {
        path: '/',
        exact: true,
        component: PageIndex,
      },
    ],
  },
];

export interface ExtendedRoute {
  route?: RouteType;
  routes?: RouteType[];
}

export interface RouteType<T extends ExtendedRoute = ExtendedRoute> extends RouteProps {
  component: React.ComponentType<T> | React.ComponentType;
  private?: boolean;
  routes?: RouteType[];
  status?: 404;
  sagasToRun?: Saga[];
}
