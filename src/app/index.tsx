import React, { FC } from 'react';
import { Route, Switch } from 'react-router-dom';
import { Helmet } from 'react-helmet-async';

import { routes, RouteType } from '@/routes';

import '@/scss/common.scss';

const App: FC = () => {
  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Helmet>

      <Switch>
        {routes.map((route, index) => (
          <Route
            key={String(index)}
            path={route.path}
            exact={route.exact}
            render={getInnerComponent(route)}
          />
        ))}
      </Switch>
    </>
  );

  function getInnerComponent(route: RouteType) {
    const RouteInner = () => <route.component route={route} routes={route.routes} />;
    return RouteInner;
  }
};

export default App;
