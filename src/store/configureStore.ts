import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import createSagaMiddleware, { END } from 'redux-saga';
import { StoreI } from 'typings/store';
import rootReducer from '../reducers';

const getStore = (preloadedState: {}) => {
  const IS_DEV = process.env.NODE_ENV !== 'production';
  const sagaMiddleware = createSagaMiddleware();
  const middleware = getDefaultMiddleware({
    immutableCheck: true,
    serializableCheck: true,
  });

  const store: StoreI = configureStore({
    reducer: rootReducer,
    middleware: [sagaMiddleware, ...middleware],
    devTools: IS_DEV,
    preloadedState,
  });

  store.runSaga = sagaMiddleware.run;
  store.close = () => store.dispatch(END);

  if (IS_DEV && module.hot) {
    module.hot.accept('../reducers', () => {
      // eslint-disable-next-line global-require
      const nextRootReducer = require('../reducers').default;
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
};

// export type AppDispatch = typeof store.dispatch;

export default getStore;
