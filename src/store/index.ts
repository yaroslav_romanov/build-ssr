import initialState from './initialState';
import getStore from './configureStore';

export { initialState, getStore };
