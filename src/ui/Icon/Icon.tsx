import React, { FC } from 'react';

interface Props {
  name: string;
  className?: string;
  width?: string;
  height?: string;
  title?: string;
  onClick?: React.MouseEventHandler;
}

const Icon: FC<Props> = ({
  name,
  title,
  className,
  width = '100%',
  height = '100%',
  onClick = () => undefined,
}) => (
  <svg width={width} height={height} onClick={onClick} className={className}>
    {title && <title>{title}</title>}
    <use xlinkHref={`#${name}`} />
  </svg>
);

export default Icon;
