import React from 'react';
import { Link } from 'react-router-dom';

import { Icon } from '@/ui';

import styles from './Header.scss';

const Header = () => (
  <header className={styles.root}>
    <div className={styles.header}>
      <Icon name="icon-logo" title="React" className={styles.logo} />
      <h2>
          Welcome to
      </h2>
      <h3>
          Razzle(SSR) + TYPESCRIPT + REDUX + SAGA + SCSS + LOADABLE +
          HELMET + SVG-SPRITE
      </h3>
    </div>
    <div className={styles.links}>
      <Link to="/">Home</Link>
      <Link to="/test1">Test1</Link>
      <Link to="/test2">Test2</Link>
    </div>
  </header>
);

export default Header;
