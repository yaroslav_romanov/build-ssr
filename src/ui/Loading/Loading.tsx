import React from 'react';
import styles from './Loading.scss';

const Loading = () => (
  <div className={styles.root}>Loading ...</div>
);

export default Loading;
