import { ChunkExtractor } from '@loadable/server';

export default function mapExtractorData(extractor: ChunkExtractor) {
  return {
    linkTags: extractor.getLinkTags() || '',
    scriptTags: extractor.getScriptTags() || '',
    styleTags: extractor.getStyleTags() || '',
  };
}
