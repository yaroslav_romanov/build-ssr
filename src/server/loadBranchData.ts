import { Request } from 'express';
import { concat } from 'lodash';
import { matchPath } from 'react-router-dom';
import { all, fork, join } from 'redux-saga/effects';
import { Saga, Task } from 'redux-saga';
import { StoreI } from 'typings/store';
import { routes, RouteType } from '@/routes';

function loadBranchData(req: Request, store: StoreI) {
  const sagasToRun = routes.reduce((sagas: Saga[], route: RouteType) => {
    const match = matchPath(req.path, route);

    if (match && route.sagasToRun) {
      return concat(sagas, route.sagasToRun);
    }

    return sagas;
  }, []);

  if (store.runSaga) {
    // eslint-disable-next-line func-names
    const result = store.runSaga(function* () {
      const tasks = yield all(sagasToRun.map((saga: Saga) => fork(saga)));
      const newTasks = tasks || [];
      yield all(newTasks.map((task: Task) => join(task)));
    });

    return result.toPromise();
  }

  return Promise.resolve();
}

export default loadBranchData;
