import { html } from 'common-tags';
import { HelmetData } from 'react-helmet';
import serialize from 'serialize-javascript';
import { StoreI } from 'typings/store';

interface Props {
  content: string;
  extractor: {
    linkTags: string;
    scriptTags: string;
    styleTags: string;
  };
  helmet: Record<keyof HelmetData, string>;
  preloaded: StoreI;
  preloadedField?: string;
}

function createHtml({
  content,
  extractor,
  helmet,
  preloaded,
  preloadedField = '__PRELOADED_STATE__',
}: Props) {
  const result: string = html`
    <!doctype html>
    <html ${helmet.htmlAttributes}>
      <head>
        ${extractor.linkTags}
        ${extractor.styleTags}
        ${helmet.base}
        ${helmet.title}
        ${helmet.meta}
        ${helmet.link}
        ${helmet.style}
        ${helmet.script}
        ${helmet.noscript}
      </head>
      <body ${helmet.bodyAttributes}>
        <div id="root">${content}</div>
        <script>
          window.${preloadedField} = ${serialize(preloaded || {}).replace(/</g, '\\u003c')};
        </script>
        ${extractor.scriptTags}
      </body>
    </html>`;
  return result;
}

export default createHtml;
