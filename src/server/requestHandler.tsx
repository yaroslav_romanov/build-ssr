import { ChunkExtractorManager, ChunkExtractor } from '@loadable/server';
import { oneLineTrim } from 'common-tags';
import { Request, Response } from 'express';
import { resolve } from 'path';
import React from 'react';
import { Provider as StoreProvider } from 'react-redux';
import { renderToString } from 'react-dom/server';
import { HelmetData } from 'react-helmet';
import { HelmetProvider } from 'react-helmet-async';
import { StaticRouterContext } from 'react-router';
import { StaticRouter } from 'react-router-dom';

import { StoreI } from 'typings/store';
import App from '@/app';
import { getStore, initialState } from '@/store';

import createHtml from './createHtml';
import loadBranchData from './loadBranchData';
import mapExtractorData from './mapExtractorData';
import mapHelmetData from './mapHelmetData';
import { ReactComponent as SpriteSvg } from '@/assets/img/sprite.svg';

const chunkExtractorOptions = {
  statsFile: resolve('build/loadable-stats.json'),
  entrypoints: ['client'],
};

const requestHandler = async (req: Request, res: Response) => {
  try {
    const store: StoreI = getStore(initialState);
    await loadBranchData(req, store);

    const chunkExtractor = new ChunkExtractor(chunkExtractorOptions);
    const routerContext: StaticRouterContext = {};
    const helmetContext = {};
    const app = (
      <ChunkExtractorManager extractor={chunkExtractor}>
        <StoreProvider store={store}>
          <HelmetProvider context={helmetContext}>
            <StaticRouter context={routerContext} location={req.url}>
              <App />
              <SpriteSvg />
            </StaticRouter>
          </HelmetProvider>
        </StoreProvider>
      </ChunkExtractorManager>
    );
    const content = renderToString(app);

    if (routerContext.url) {
      res.redirect(routerContext.statusCode || 301, routerContext.url);
    } else {
      const html = createHtml({
        content,
        extractor: mapExtractorData(chunkExtractor),
        helmet: mapHelmetData(helmetContext as HelmetData),
        preloaded: store.getState(),
      });
      res.status(routerContext.statusCode || 200).send(oneLineTrim(html));
    }
  } catch (error) {
    res.status(500);
    if (process.env.NODE_ENV === 'development') {
      console.log(error);
      res.json(error);
    } else {
      res.send('Server Error');
    }
  }
};

export default requestHandler;
