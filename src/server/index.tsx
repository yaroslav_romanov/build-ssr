import express from 'express';
import requestHandler from './requestHandler';

const server = express().disable('x-powered-by');

if (process.env.RAZZLE_PUBLIC_DIR) {
  server.use(express.static(process.env.RAZZLE_PUBLIC_DIR));
}

server.get('/*', requestHandler);

export default server;
