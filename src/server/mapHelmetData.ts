import {
  HelmetData,
  HelmetDatum,
  HelmetHTMLBodyDatum,
  HelmetHTMLElementDatum,
} from 'react-helmet';

export default function mapHelmetData({
  base,
  bodyAttributes,
  htmlAttributes,
  link,
  meta,
  noscript,
  script,
  style,
  title,
  titleAttributes,
}: HelmetData): Record<keyof HelmetData, string> {
  return {
    base: convert(base),
    bodyAttributes: convert(bodyAttributes),
    htmlAttributes: convert(htmlAttributes),
    link: convert(link),
    meta: convert(meta),
    noscript: convert(noscript),
    script: convert(script),
    style: convert(style),
    title: convert(title),
    titleAttributes: convert(titleAttributes),
  };
}

function convert(
  data: HelmetDatum | HelmetHTMLBodyDatum | HelmetHTMLElementDatum,
) {
  return (data && data.toString()) || '';
}
