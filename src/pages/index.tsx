import React from 'react';
import loadable from '@loadable/component';

import { Loading } from '@/ui';

const PageIndex = loadable(() => import('@/pages/PageIndex'), { fallback: <Loading /> });
const PageTest1 = loadable(() => import('@/pages/PageTest1'), { fallback: <Loading /> });
const PageTest2 = loadable(() => import('@/pages/PageTest2'), { fallback: <Loading /> });
const LayoutIndex = loadable(() => import('@/pages/LayoutIndex'), { fallback: <Loading /> });

export {
  LayoutIndex,
  PageIndex,
  PageTest1,
  PageTest2,
};
