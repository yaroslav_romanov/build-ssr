import React from 'react';
import { useDispatch } from 'react-redux';
import { authFetchRequesting } from '@/reducers/auth';

const PageIndex = () => {
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(authFetchRequesting());
  }, [dispatch]);

  return <h1>PageIndex</h1>;
};

export default PageIndex;
