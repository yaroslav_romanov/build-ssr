import React, { FC } from 'react';
import { Route, Switch } from 'react-router-dom';

import { Header } from '@/ui';
import { RouteType, ExtendedRoute } from '@/routes';

import styles from './LayoutIndex.scss';

const LayoutIndex: FC<ExtendedRoute> = ({ routes = [] }) => (
  <div className={styles.root}>
    <Header />
    <main className={styles.content}>
      <Switch>
        {routes.map((route: RouteType, index: number) => (
          <Route
            key={String(index)}
            path={route.path}
            exact={route.exact}
            component={route.component}
          />
        ))}
      </Switch>
    </main>
  </div>
);

export default LayoutIndex;
