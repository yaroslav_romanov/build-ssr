const LoadableBabelPlugin = require('@loadable/babel-plugin');
const LoadableWebpackPlugin = require('@loadable/webpack-plugin');
const path = require('path');
const babelPresetRazzle = require('razzle/babel');

module.exports = {
  modify: (baseConfig, { dev, target }) => {
    const config = Object.assign({}, baseConfig);

    // Resolve

    config.resolve.alias['@'] = path.resolve(__dirname, 'src');
    config.resolve.extensions = config.resolve.extensions.concat([
      '.ts',
      '.tsx',
    ]);

    // --- Chunks ---

    if (target === 'web') {
      const filename = path.resolve(__dirname, 'build');

      config.plugins = [
        ...config.plugins,
        new LoadableWebpackPlugin({
          outputAsset: false,
          writeToDisk: { filename },
        }),
      ];

      config.output.filename = dev
        ? 'static/js/[name].js'
        : 'static/js/[name].[chunkhash:8].js';

      config.node = { fs: 'empty' }; // fix "Cannot find module 'fs'" problem.
      config.optimization = Object.assign({}, config.optimization, {
        runtimeChunk: true,
        splitChunks: {
          chunks: 'all',
          name: dev,
        },
      });
    }

    // --- Return ---

    return config;
  },

  modifyBabelOptions: () => ({
    babelrc: false,
    presets: [babelPresetRazzle],
    plugins: [LoadableBabelPlugin],
  }),

  plugins: [
    'svg-react-component',
    {
      name: 'typescript',
      options: {
        useBabel: true,
        forkTsChecker: {
          eslint: true,
          tslint: undefined,
        },
      },
    },
    {
      name: 'scss',
      options: {
        css: {
          dev: {
            sourceMap: true,
            importLoaders: 1,
            modules: true,
            localIdentName: '[name]__[local]--[hash:base64:5]',
          },
          prod: {
            sourceMap: false,
            importLoaders: 1,
            modules: true,
            minimize: true,
          },
        },
      },
    },
  ],
};
