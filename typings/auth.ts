interface UserI {
  firstName?: string;
  lastName?: string;
}

export interface AuthI {
  fetchingState: string;
  creatingState: string;
  loadingState: string;
  error: null;
  user: UserI;
}
