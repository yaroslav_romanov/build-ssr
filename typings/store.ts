import { Store } from 'redux';
import { Saga, Task } from 'redux-saga';
import { AuthI } from './auth';

export interface StoreI extends Store {
  auth?: AuthI;
  runSaga?: (s: Saga) => Task;
  close?: () => void;
}
