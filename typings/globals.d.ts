// <reference types="react-scripts" />

interface Window {
  __PRELOADED_STATE__: {};
}

declare module '*.svg' {
  import { SFC, SVGProps } from 'react';

  const content: string;

  export const ReactComponent: SFC<SVGProps<SVGSVGElement>>;
  export default content;
}

declare module '*.scss' {
  const classes: { [key: string]: string }
  export default classes
}

declare module 'react-async-ssr' {
  import { ReactElement } from 'react';
  export function renderToStringAsync(element: ReactElement): Promise<string>;
}

declare module 'react-async-ssr/symbols';
